/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;

/**
 *
 * @author ripgg
 */
public class JSliderExample extends JFrame {

    public JSliderExample() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 750, 150);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        JSliderExample frame = new JSliderExample();
        frame.pack();
        frame.setVisible(true);
    }
}
