/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.javaswingcomponent;

import java.awt.Frame;
import java.awt.Image;
import java.awt.Toolkit;

/**
 *
 * @author ripgg
 */
public class ExampleTochangeTitleBarIconInJavaAWT {

    ExampleTochangeTitleBarIconInJavaAWT() {
        Frame f = new Frame();
        Image icon = Toolkit.getDefaultToolkit().getImage("D:\\Cute-kittens-12929201-1600-1200.jpg");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(400, 400);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new ExampleTochangeTitleBarIconInJavaAWT();
    }
}
