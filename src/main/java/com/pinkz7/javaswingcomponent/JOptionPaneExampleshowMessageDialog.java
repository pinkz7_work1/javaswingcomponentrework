/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pinkz7.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author ripgg
 */
public class JOptionPaneExampleshowMessageDialog {

    JFrame f;

    JOptionPaneExampleshowMessageDialog() {
        f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JOptionPane.showMessageDialog(f, "Hello, Welcome to Javatpoint.");
    }

    public static void main(String[] args) {
        new JOptionPaneExampleshowMessageDialog();
    }
}
